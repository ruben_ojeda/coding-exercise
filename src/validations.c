#include "validations.h"

#include <string.h>

bool is_valid_repetition(const NUMERAL_INDEX_TYPE index, const char letter, const char next_letter, uint8_t* repetitions)
{
    if(letter == next_letter)
    {
        (*repetitions)++;
        if((*repetitions) > numeral_info[index].allowed_repetitions)
        {
            return false;
        }
    }
    else
    {
        (*repetitions) = 1;
    }
    return true;
}

bool is_valid_numeral_argument(const char* numeral)
{
    if((NULL == numeral) || (0 == strlen(numeral)))
    {
        return false;
    }
    return true;
}

bool is_valid_numeral_subtraction(const NUMERAL_INDEX_TYPE index, const NUMERAL_INDEX_TYPE next_index)
{
    if(INVALID_NUMERAL != next_index)
    {
        if(numeral_info[index].value < numeral_info[next_index].value)
        {
            if(is_five_numeral_index(index))
            {
                return false;
            }
            else
            {
                if(1 == numeral_info[index].letter_size)
                {
                    return false;
                }
            }
        }
    }
    return true;
}

bool is_valid_numeral(const char* numeral, uint8_t* repetitions)
{
    NUMERAL_INDEX_TYPE index = numeral_index(numeral);

    if( (INVALID_NUMERAL == index) ||
        (!is_valid_repetition(index, numeral[0], numeral[1], repetitions)) ||
        (!is_valid_numeral_subtraction(index, numeral_index(&numeral[1]))))
    {
        return false;
    }
    return true;
}

bool is_valid_roman_numeral(const char* numeral)
{
    NUMERAL_INDEX_TYPE index;
    uint8_t letter, repetitions, letters;

    if(!is_valid_numeral_argument(numeral))
    {
        return false;
    }
    for(letter = 0, repetitions = 1, letters = strlen(numeral); letter < letters; letter += numeral_info[index].letter_size)
    {
        index = numeral_index(&numeral[letter]);
        if(!is_valid_numeral(&numeral[letter], &repetitions))
        {
            return false;
        }
    }
    return true;
}

bool function_paramenters_are_valid(const char* numeral1, const char* numeral2, char result[16])
{
    if(is_valid_roman_numeral(numeral1) && is_valid_roman_numeral(numeral2) && (NULL != result))
    {
        return true;
    }
    return false;
}
