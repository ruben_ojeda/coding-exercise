#ifndef VALIDATIONS_H
#define VALIDATIONS_H

#include "conversions.h"

bool is_valid_repetition(const NUMERAL_INDEX_TYPE index, const char letter, const char next_letter, uint8_t* repetitions);
bool is_valid_numeral_subtraction(const NUMERAL_INDEX_TYPE index, const NUMERAL_INDEX_TYPE next_index);
bool is_valid_roman_numeral(const char* numeral);
bool function_paramenters_are_valid(const char* numeral1, const char* numeral2, char result[16]);
bool is_valid_numeral_argument(const char* numeral);
bool is_valid_numeral(const char* numeral, uint8_t* repetitions);

#endif /* VALIDATIONS_H */
